﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pJump : MonoBehaviour
{
    public float jumpForce;
    public float jumpTime;
    private float jTimeCounter;
    private bool jumpStop;
    public bool grounded;
    private bool doubleJ=true;
    private Rigidbody2D rb;
    Animator animator;

    [SerializeField] private Transform groundCheck;
    [SerializeField] private float radiusCirc=0.03f;
    [SerializeField] private LayerMask whatIsGround;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        jTimeCounter = jumpTime;
    }


    void Update()
    {
        //Saltos
        grounded = Physics2D.OverlapCircle(groundCheck.position, radiusCirc, whatIsGround);
        if (grounded)
        {
            jTimeCounter = jumpTime;
            doubleJ = true;
        }
        if (Input.GetButtonDown("Jump") && grounded)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            jumpStop = false;
        }
        if (Input.GetButtonDown("Jump") && !jumpStop && jTimeCounter > 0)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            jTimeCounter -= Time.deltaTime;
        }
        if (Input.GetButtonUp("Jump"))
        {
            jTimeCounter = 0;
            jumpStop = true;
        }
        if (Input.GetButtonDown("Jump") && !grounded && doubleJ)
        {
            rb.velocity = new Vector2(rb.velocity.x, jumpForce);
            jumpStop = false;
            doubleJ = false;
        }
    }

    private void FixedUpdate()
    {
        animator.SetBool("jump", grounded);
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(groundCheck.position, radiusCirc);
    }
}
